
let trainer = {}
console.log(trainer);

// add the objects properties 

trainer.name = 'ask ketchum';
trainer.age = 10;
trainer.pokemon = ['pikachu', 'charizard', 'squirel', 'bulbasaur'];
trainer.friends = {
    kanto: ['Brock', 'Misty'],
    hoenn: ['May', 'Max']
};

// METHODS 
trainer.talk = function() {
    console.log('pikachu, I choose you!')
};

// Check all of the properties and methods were properly added
console.log(trainer);

// access object properties and methods
console.log('Result of dot notation')
console.log(trainer.name);

// access object properties using square bracket notation
console.log('Result of square bracket notation: ')
console.log(trainer['pokemon'])

// access the trainer 'talk' method 
console.log('result of talk method:')
trainer.talk()

// construction function 
function Pokemon (name, level){

    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name)
        target.health -= this.attack;
        console.log(target.name + "'s health is now reduce to " + target.health);

        if (target.health <= 0) {
            target.faint()
        }
    };
    this.faint = function(){
        console.log(this.name + " fainted")
    }
}

let pikachu = new Pokemon('pikachu', 12);
console.log(pikachu);

let geodude = new Pokemon('Geodude', 8)
console.log(geodude);

let mewto = new Pokemon('Mewto', 100)
console.log(mewto);

// invoke the tackle method and target a diff object

geodude.tackle(pikachu);
console.log(geodude);

// invoke the tackle
mewto.tackle(geodude);
console.log(mewto);